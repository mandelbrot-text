module Mandelbrot.Text.Compat
  ( module Text.Parsec
  , Parser
  , intercalate
  )
  where

import Control.Monad.Identity
  ( Identity()
  )
import Data.List
  ( intercalate
  )

import Text.Parsec hiding
  ( parse
  )

type Parser t = ParsecT String () Identity t
