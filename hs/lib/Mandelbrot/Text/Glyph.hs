module Mandelbrot.Text.Glyph
  ( Glyph(..)
  ) where

data Glyph = Dot | Bra | Ket | Over | ArrowRight | Star
  deriving (Eq, Ord, Enum, Bounded, Read, Show)
