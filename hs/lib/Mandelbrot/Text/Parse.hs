{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}
module Mandelbrot.Text.Parse
  ( Parser
  , Parse(..)
  , ConciseAddress(..)
  , unConciseAddress
  , pAddress, pAddress'
  , pAngledAddress, pAngledAddress'
  , pBinaryAngle, pBinaryAngle'
  , pBlock, pBlock'
  , pConciseAddress, pConciseAddress'
  , pExternalAngle, pExternalAngle'
  , pInternalAddress, pInternalAddress'
  , pInternalAngle, pInternalAngle'
  , pKneading, pKneading'
  ) where

import Prelude hiding
  ( Rational
  )

import Control.Monad
  ( guard
  )
import Data.Char
  ( digitToInt
  )
import Data.List
  ( foldl'
  )
import Data.Maybe
  ( isJust
  , isNothing
  , catMaybes
  )
import Mandelbrot.Text.Compat

import Mandelbrot.Symbolics
  ( AngledAddress(..)
  , Block()
  , compact
  , fromList
  , ExternalAngle(..)
  , BinaryAngle
  , binaryAngle
  , InternalAddress(..)
  , InternalAngle(..)
  , Kneading(..)
  , Periods(..)
  , Rational
  , (%)
  , denominator
  )

import Mandelbrot.Text.Glyph
  ( Glyph(..)
  )
import Mandelbrot.Text.Plain
  ( plain
  )

class Parse t where
  parser :: Parser t
  parse' :: String -> Maybe t
  parse' s = case runParser parser () "" s of
    Left _ -> Nothing
    Right t -> Just t
  parse :: String -> t
  parse s = case runParser parser () "" s of
   Left e -> error $ show ("parse failed", e, s)
   Right t -> t

instance Parse AngledAddress where
  parser = choice [ try angled, unangled ] <?> "angled address"
    where
      unangled = Unangled `fmap` parser <?> "period"
      angled = do
        n <- parser <?> "period"
        _ <- string "_"
        r <- parser
        _ <- string (plain ArrowRight)
        a <- parser
        return $ Angled n r a

instance Parse BinaryAngle where
  parser = do
    _ <- string (plain Dot)
    pre <- parser <?> "preperiodic bits"
    _ <- string (plain Bra)
    per <- parser <?> "periodic bits"
    _ <- string (plain Ket)
    return $ binaryAngle pre per

instance Parse Block where
  parser = fromList `fmap` many bit <?> "bits"

bit :: Parser Bool
bit = choice
  [ string "0" >> return False
  , string "1" >> return True
  ] <?> "bit"

instance Parse ExternalAngle where
  parser = ExternalAngle `fmap` parser <?> "external angle"

instance Parse Int where
  parser = base10 `fmap` map digitToInt `fmap` many1 digit <?> "int"

instance Parse Integer where
  parser = base10 `fmap` map (toInteger . digitToInt) `fmap` many1 digit <?> "integer"

base10 :: Num a => [a] -> a
base10 = foldl' (\x y -> 10 * x + y) 0

instance Parse InternalAddress where
  parser = InternalAddress `fmap` sepBy1 (parser <?> "period") (string (plain ArrowRight)) <?> "internal address"

instance Parse InternalAngle where
  parser = InternalAngle `fmap` parser <?> "internal angle"

instance Parse Kneading where
  parser = do
    pre <- many knead <?> "kneading preperiodic part"
    per <- do
      _ <- string (plain Bra)
      per' <- many1 knead <?> "kneading periodic part"
      _ <- string (plain Ket)
      return per'
    case () of
      _ | null pre &&
          all isJust per -> return . Periodic . compact . fromList . catMaybes $ per
        | null pre &&
          all isJust (init per) &&
          isNothing (last per) -> return . StarPeriodic . fromList . catMaybes $ per
        | all isJust pre &&
          all isJust per -> return $ PrePeriodic (fromList . catMaybes $ pre) (compact . fromList . catMaybes $ per)  -- FIXME might not be canonical?
      _ -> fail "invalid kneading sequence"

knead :: Parser (Maybe Bool)
knead = choice
  [ string "0" >> return (Just False)
  , string "1" >> return (Just True)
  , string (plain Star) >> return Nothing
  ] <?> "kneading item"

instance Parse Periods where
  parser = do
    pp <- base10 `fmap` map digitToInt `fmap` many digit <?> "preperiod"
    _ <- string "p"
    p <- base10 `fmap` map digitToInt `fmap` many1 digit <?> "period"
    guard (pp >= 0)
    guard (p >= 1)
    return $ Periods (pp, p)

instance Parse Rational where
  parser = (do
    n <- parser <?> "numerator"
    _ <- string (plain Over)
    d <- parser <?> "denominator"
    guard (d /= 0)
    return $ n % d) <?> "rational"

newtype ConciseAddress = ConciseAddress AngledAddress
  deriving (Read, Show, Eq, Ord)

unConciseAddress :: ConciseAddress -> AngledAddress
unConciseAddress (ConciseAddress a) = a

data Token = Number Int | Fraction InternalAngle
  deriving (Read, Show, Eq, Ord)

pToken :: Parser Token -- FIXME something not quite right here...
pToken = choice
  [ try (Fraction `fmap` parser) <?> "fraction"
  , Number `fmap` parser <?> "number"
  ] <?> "token"

pTokens :: Parser [Token]
pTokens = pToken `sepBy` spaces

instance Parse ConciseAddress where
  parser = do
    ts <- pTokens
    ConciseAddress `fmap` accum 1 ts
    where
      accum p [] = return $ Unangled (fromIntegral p)
      accum _ [Number n] = return $ Unangled (fromIntegral n)
      accum _ (Number n : ts@(Number _ : _)) = do
        a <- accum n ts
        return $ Angled (fromIntegral n) (1%2) a
      accum _ (Number n : Fraction r : ts) = do
        a <- accum (n * fromInteger (denominator r)) ts
        return $ Angled (fromIntegral n) r a
      accum p (Fraction r : ts) = do
        a <- accum (p * fromInteger (denominator r)) ts
        return $ Angled (fromIntegral p) r a

pAddress' :: String -> Maybe AngledAddress
pAddress' = fmap unConciseAddress . parse'

pAddress :: String -> AngledAddress
pAddress = unConciseAddress . parse

pAngledAddress' :: String -> Maybe AngledAddress
pAngledAddress' = parse'

pAngledAddress :: String -> AngledAddress
pAngledAddress = parse

pBinaryAngle' :: String -> Maybe BinaryAngle
pBinaryAngle' = parse'

pBinaryAngle :: String -> BinaryAngle
pBinaryAngle = parse

pBlock' :: String -> Maybe Block
pBlock' = parse'

pBlock :: String -> Block
pBlock = parse

pConciseAddress' :: String -> Maybe ConciseAddress
pConciseAddress' = parse'

pConciseAddress :: String -> ConciseAddress
pConciseAddress = parse

pExternalAngle' :: String -> Maybe ExternalAngle
pExternalAngle' = parse'

pExternalAngle :: String -> ExternalAngle
pExternalAngle = parse

pInternalAddress' :: String -> Maybe InternalAddress
pInternalAddress' = parse'

pInternalAddress :: String -> InternalAddress
pInternalAddress = parse

pInternalAngle' :: String -> Maybe InternalAngle
pInternalAngle' = parse'

pInternalAngle :: String -> InternalAngle
pInternalAngle = parse

pKneading' :: String -> Maybe Kneading
pKneading' = parse'

pKneading :: String -> Kneading
pKneading = parse
