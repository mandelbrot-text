{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}
{- |
Pretty-printing in plain text.
-}
module Mandelbrot.Text.Plain
  ( Plain(..)
  , pprint
  ) where

import Prelude hiding
  ( Rational
  )
import qualified Data.Ratio as Ratio
import Data.Complex
  ( Complex
  )

import Mandelbrot.Symbolics
  ( AngledAddress(..)
  , Block
  , toList
  , BinaryAngle(..)
  , ExternalAngle(..)
  , InternalAddress(..)
  , InternalAngle(..)
  , Kneading(..)
  , Periods(..)
  , Rational
  , numerator
  , denominator
  )

import Mandelbrot.Text.Glyph
  ( Glyph(..)
  )

import Mandelbrot.Text.Compat
  ( intercalate
  )

class Plain t where
  plain :: t -> String
  plain x = plainPrec 0 x ""
  plainPrec :: Int -> t -> String -> String
  plainPrec _ x s = plain x ++ s
  plainList :: [t] -> String -> String
  plainList xs s = "[" ++ intercalate "," (map plain xs) ++ "]" ++ s

instance Plain Glyph where
  plain Dot        = "."
  plain Bra        = "("
  plain Ket        = ")"
  plain Over       = "/"
  plain ArrowRight = "->" -- "\8594"
  plain Star       = "*"  -- "\9733"

instance Plain AngledAddress where
  plain (Unangled n) = show n
  plain (Angled n r a) = show n ++ "_" ++ plain r ++ plain ArrowRight ++ plain a

instance Plain BinaryAngle where
  plain (BinaryAngle pre per)
    = plain Dot ++ plain pre ++ plain Bra ++ plain per ++ plain Ket

instance Plain Block where
  plain = map bit . toList
    where
      bit False = '0'
      bit True  = '1'

instance Plain ExternalAngle where
  plain (ExternalAngle r) = plain r

instance Plain InternalAddress where
  plain (InternalAddress xs) = intercalate (plain ArrowRight) (map show xs)

instance Plain InternalAngle where
  plain (InternalAngle r) = plain r

instance Plain Kneading where
  plain (PrePeriodic pre per) = plain pre ++ plain Bra ++ plain per ++ plain Ket
  plain (StarPeriodic per) = plain Bra ++ plain per ++ plain Star ++ plain Ket
  plain (Periodic per) = plain Bra ++ plain per ++ plain Ket

instance Plain Periods where
  plain (Periods (pp, p))
    | pp == 0 = "p" ++ show p
    | otherwise = show pp ++ "p" ++ show p

instance Plain Rational where
  plain x = show (numerator x) ++ plain Over ++ show (denominator x)

instance Plain Ratio.Rational where
  plain x = show (numerator x) ++ plain Over ++ show (denominator x)

appPrec :: Int
appPrec = 10

appPrec1 :: Int
appPrec1 = 11

instance Plain Char where
  plain = show
  plainList = showList

instance Plain Bool where plain = show
instance Plain Int where plain = show
instance Plain Integer where plain = show
instance Plain Double where plain = show
instance (RealFloat r, Show r) => Plain (Complex r) where plainPrec = showsPrec -- FIXME should use Plain instead of Show

instance Plain a => Plain (Maybe a) where
  plainPrec _ Nothing s = showString "Nothing" s
  plainPrec p (Just x) s =
    (showParen (p > appPrec) $
      showString "Just " .
      plainPrec appPrec1 x) s

instance Plain a => Plain [a] where
  plainPrec _ xs s = plainList xs s

instance (Plain a, Plain b) => Plain (Either a b) where
  plainPrec p (Left x) s =
    (showParen (p > appPrec) $
      showString "Left " .
      plainPrec appPrec1 x) s
  plainPrec p (Right x) s =
    (showParen (p > appPrec) $
      showString "Right " .
      plainPrec appPrec1 x) s

tuple :: [String->String] -> String->String
tuple ss = ('(':) . foldr1 (\s r -> s . (',':) . r) ss . (')':)

q :: Plain t => t -> String->String
q t s = plainPrec 0 t s

instance (Plain a, Plain b) => Plain (a,b) where
  plainPrec _ (a,b) s = tuple [q a, q b] s
instance (Plain a, Plain b, Plain c) => Plain (a,b,c) where
  plainPrec _ (a,b,c) s = tuple [q a, q b, q c] s
instance (Plain a, Plain b, Plain c, Plain d) => Plain (a,b,c,d) where
  plainPrec _ (a,b,c,d) s = tuple [q a, q b, q c, q d] s
instance (Plain a, Plain b, Plain c, Plain d, Plain e) => Plain (a,b,c,d,e) where
  plainPrec _ (a,b,c,d,e) s = tuple [q a, q b, q c, q d, q e] s

pprint :: Plain a => a -> IO ()
pprint = putStrLn . plain
