{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}
{- |
Pretty-printing in HTML.
-}
module Mandelbrot.Text.HTML
  ( HTML(..)
  ) where

import Prelude hiding
  ( Rational
  )
import qualified Data.Ratio as Ratio

import Mandelbrot.Symbolics
  ( AngledAddress(..)
  , BinaryAngle(..)
  , ExternalAngle(..)
  , InternalAddress(..)
  , InternalAngle(..)
  , Kneading(..)
  , Periods(..)
  , Rational
  , numerator
  , denominator
  )

import Mandelbrot.Text.Glyph
  ( Glyph(..)
  )
import Mandelbrot.Text.Plain
  ( plain
  )

import Mandelbrot.Text.Compat
  ( intercalate
  )

class HTML t where
  html :: t -> String

instance HTML Glyph where
  html Dot        = "<span class='m-dot'>" ++ plain Dot ++ "</span>"
  html Bra        = "<span class='m-bra'>" ++ plain Bra ++ "</span>"
  html Ket        = "<span class='m-ket'>" ++ plain Ket ++ "</span>"
  html Over       = "<span class='m-over'>" ++ plain Over ++ "</span>"
  html ArrowRight = "<span class='m-arrowright'>&#8594;</span>"
  html Star       = "<span class='m-star'>&#9733;</span>"

instance HTML AngledAddress where
  html aa = "<span class='m-angledaddress'>" ++ h aa ++ "</span>"
    where
      h (Unangled n) = "<span class='m-period'>" ++ show n ++ "</span>"
      h (Angled n r a)
        =  "<span class='m-period'>"
        ++ show n
        ++ "</span><sub>"
        ++ html r
        ++ "</sub>"
        ++ html ArrowRight
        ++ h a

instance HTML BinaryAngle where
  html (BinaryAngle pre per)
    =  "<span class='m-binaryangle'>"
    ++ html Dot
    ++ "<span class='m-preperiodic'>"
    ++ plain pre
    ++ "</span><span class='m-periodic'>"
    ++ plain per
    ++ "</span></span>"

instance HTML ExternalAngle where
  html (ExternalAngle r)
    = "<span class='m-externalangle'>" ++ html r ++ "</span>"

instance HTML InternalAddress where
  html (InternalAddress xs)
    =  "<span class='m-internaladdress'>"
    ++ intercalate (html ArrowRight) (map h xs)
    ++ "</span>"
    where h x = "<span class='m-period'>" ++ show x ++ "</span>"

instance HTML InternalAngle where
  html (InternalAngle r)
    = "<span class='m-internalangle'>" ++ html r ++ "</span>"

instance HTML Kneading where
  html (PrePeriodic pre per)
    =  "<span class='m-kneading'><span class='m-preperiodic'>"
    ++ plain pre
    ++ "</span><span class='m-periodic'>"
    ++ plain per
    ++ "</span></span>"
  html (StarPeriodic per)
    =  "<span class='m-kneading'><span class='m-periodic'>"
    ++ plain per
    ++ html Star
    ++ "</span></span>"
  html (Periodic per)
    =  "<span class='m-kneading'><span class='m-periodic'>"
    ++ plain per
    ++ "</span></span>"

instance HTML Periods where
  html (Periods (pp, p))
    | pp == 0 = "<span class='m-periods'><span class='m-p'>p</span>" ++
        "<span class='m-period'>" ++ show p ++ "</span></span>"
    | otherwise = "<span class='m-periods'><span class='m-preperiod'>" ++
        show pp ++ "</span><span class='m-p'>p</span><span class='m-period'>" ++
        show p ++ "</span></span>"

instance HTML Rational where
  html x
    =  "<span class='m-rational'><span class='m-numerator'>"
    ++ show (numerator x)
    ++ "</span>"
    ++ html Over
    ++ "<span class='m-denominator'>"
    ++ show (denominator x)
    ++ "</span></span>"

instance HTML Ratio.Rational where
  html x
    =  "<span class='m-rational'><span class='m-numerator'>"
    ++ show (numerator x)
    ++ "</span>"
    ++ html Over
    ++ "<span class='m-denominator'>"
    ++ show (denominator x)
    ++ "</span></span>"
