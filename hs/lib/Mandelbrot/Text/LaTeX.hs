{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}
{- |
Pretty-printing in LaTeX.
-}
module Mandelbrot.Text.LaTeX
  ( LaTeX(..)
  ) where

import Prelude hiding
  ( Rational
  )
import qualified Data.Ratio as Ratio

import Mandelbrot.Symbolics
  ( AngledAddress(..)
  , BinaryAngle(..)
  , ExternalAngle(..)
  , InternalAddress(..)
  , InternalAngle(..)
  , Kneading(..)
  , Periods(..)
  , Rational
  , numerator
  , denominator
  )

import Mandelbrot.Text.Glyph
  ( Glyph(..)
  )
import Mandelbrot.Text.Plain
  ( plain
  )

import Mandelbrot.Text.Compat
  ( intercalate
  )

class LaTeX t where
  latex :: t -> String

instance LaTeX Glyph where
  latex Dot        = "."
  latex Bra        = "\\left("
  latex Ket        = "\\right)"
  latex Over       = "/"
  latex ArrowRight = " \\to "
  latex Star       = " \\star "

instance LaTeX AngledAddress where
  latex (Unangled n) = show n
  latex (Angled n r a)
    = show n ++ "_{" ++ latex r ++ "}" ++ latex ArrowRight ++ latex a

instance LaTeX BinaryAngle where
  latex (BinaryAngle pre per)
    = latex Dot ++ plain pre ++ "\\overline{" ++ plain per ++ "}"

instance LaTeX ExternalAngle where
  latex (ExternalAngle r) = latex r

instance LaTeX InternalAddress where
  latex (InternalAddress xs)
    =  intercalate (latex ArrowRight) (map show xs)

instance LaTeX InternalAngle where
  latex (InternalAngle r) = latex r

instance LaTeX Kneading where
  latex (PrePeriodic pre per)
    = plain pre ++ "\\overline{" ++ plain per ++ "}"
  latex (StarPeriodic per)
    = "\\overline{" ++ plain per ++ latex Star ++ "}"
  latex (Periodic per)
    = "\\overline{" ++ plain per ++ "}"

instance LaTeX Periods where
  latex (Periods (pp, p))
    | pp == 0 = "\\mathbf{p}" ++ show p
    | otherwise = show pp ++ "\\mathbf{p}" ++ show p

instance LaTeX Rational where
  latex x
    = "\\frac{\\hfill " ++ show (numerator x) ++ "}{" ++ show (denominator x) ++ "}"

instance LaTeX Ratio.Rational where
  latex x
    = "\\frac{\\hfill " ++ show (numerator x) ++ "}{" ++ show (denominator x) ++ "}"

instance LaTeX Int where latex = show
instance LaTeX Integer where latex = show
instance LaTeX Double where latex = show
