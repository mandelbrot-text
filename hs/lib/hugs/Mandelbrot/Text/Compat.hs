module Mandelbrot.Text.Compat
  ( module Text.ParserCombinators.Parsec
  , Parser
  , intercalate
  )
  where

import Control.Monad.Instances ()
import Data.List (intersperse)

import Text.ParserCombinators.Parsec hiding
  ( Parser
  , parse
  )

type Parser t = CharParser () t

intercalate x = concat . intersperse x
